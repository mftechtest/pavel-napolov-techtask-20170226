# Install
1. Install vagrant
2. vagrant up
3. Update hosts file on vagrant host: 127.0.0.1 mftest.local
4. Check install http://mftest.local:8989

# Usage
1. Get recipes for current date http://mftest.local:8989/lunch
2. Get recipes for specific date http://mftest.local:8989/lunch/2017-02-06
3. Get recipes and post a tweet http://mftest.local:8989/lunch/2017-02-06?tweet=true
4. Twitter: https://twitter.com/mftechtest
