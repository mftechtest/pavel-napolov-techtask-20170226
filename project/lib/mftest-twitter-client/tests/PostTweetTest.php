<?php
use Dotenv\Dotenv;
use PHPUnit\Framework\TestCase;
use MFTest\TwitterClient\PostTweet;

class PostTweetTest extends TestCase
{
    public function __construct()
    {
        (new Dotenv($GLOBALS['PROJECT_ROOT']))->load();
        parent::__construct();
    }

    public function test_request_data()
    {
        $tweet = (new PostTweet)
            ->setUrl(getenv('TWITTER_POST_URL'))
            ->setApiKey(getenv('TWITTER_API_KEY'))
            ->setApiSecret(getenv('TWITTER_API_SECRET'))
            ->setOauthKey(getenv('TWITTER_ACCESS_TOKEN'))
            ->setOauthSecret(getenv('TWITTER_ACCESS_SECRET'))
            ->setText('test '.time());

        $this->assertInstanceOf(PostTweet::class, $tweet);
        $data = $tweet->getRequestData();

        $this->assertInternalType('array', $data);

        foreach (['oauth_consumer_key',
                  'oauth_nonce',
                  'oauth_signature_method',
                  'oauth_timestamp',
                  'oauth_token',
                  'oauth_version',
                  'status'] as $v) {
            $this->assertArrayHasKey($v, $data);
            $this->assertFalse(empty($data[$v]));
        }

        $this->assertFalse(empty($tweet->getSignature($data)));
    }

    public function test_request()
    {
        $tweet = (new PostTweet)
            ->setUrl(getenv('TWITTER_POST_URL'))
            ->setApiKey(getenv('TWITTER_API_KEY'))
            ->setApiSecret(getenv('TWITTER_API_SECRET'))
            ->setOauthKey(getenv('TWITTER_ACCESS_TOKEN'))
            ->setOauthSecret(getenv('TWITTER_ACCESS_SECRET'))
            ->setText('test '.time());

        $result = $tweet->result();
        $this->assertInstanceOf(stdClass::class, $result);
        $this->assertObjectHasAttribute('id', $result);
        $this->assertObjectHasAttribute('created_at', $result);
    }
}