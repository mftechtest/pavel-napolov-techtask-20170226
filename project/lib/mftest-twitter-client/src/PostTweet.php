<?php
namespace MFTest\TwitterClient;

use Curl\Curl;

class PostTweet
{
    /**
     * @var string
     */
    protected $url;

    /**
     * @var string
     */
    protected $api_key;

    /**
     * @var string
     */
    protected $api_secret;

    /**
     * @var string
     */
    protected $oauth_key;

    /**
     * @var string
     */
    protected $oauth_secret;

    /**
     * @var string
     */
    protected $text;

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string $url
     * @return PostTweet
     */
    public function setUrl(string $url): PostTweet
    {
        $this->url = $url;

        return $this;
    }

    /**
     * @return string
     */
    public function getApiKey(): string
    {
        return $this->api_key;
    }

    /**
     * @param string $api_key
     * @return PostTweet
     */
    public function setApiKey(string $api_key): PostTweet
    {
        $this->api_key = $api_key;

        return $this;
    }

    /**
     * @return string
     */
    public function getApiSecret(): string
    {
        return $this->api_secret;
    }

    /**
     * @param string $api_secret
     * @return PostTweet
     */
    public function setApiSecret(string $api_secret): PostTweet
    {
        $this->api_secret = $api_secret;

        return $this;
    }

    /**
     * @return string
     */
    public function getOauthKey(): string
    {
        return $this->oauth_key;
    }

    /**
     * @param string $oauth_key
     * @return PostTweet
     */
    public function setOauthKey(string $oauth_key): PostTweet
    {
        $this->oauth_key = $oauth_key;

        return $this;
    }

    /**
     * @return string
     */
    public function getOauthSecret(): string
    {
        return $this->oauth_secret;
    }

    /**
     * @param string $oauth_secret
     * @return PostTweet
     */
    public function setOauthSecret(string $oauth_secret): PostTweet
    {
        $this->oauth_secret = $oauth_secret;

        return $this;
    }

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @param string $text
     * @return PostTweet
     */
    public function setText(string $text): PostTweet
    {
        $this->text = $text;

        return $this;
    }

    /**
     * @return array
     */
    public function getRequestData(): array
    {
        return [
            'oauth_consumer_key' => $this->getApiKey(),
            'oauth_nonce' => md5(microtime().mt_rand()),
            'oauth_signature_method' => 'HMAC-SHA1',
            'oauth_timestamp' => time(),
            'oauth_token' => $this->getOauthKey(),
            'oauth_version' => '1.0',
            'status' => $this->getText(),
        ];
    }

    /**
     * @param $request_data
     * @return string
     */
    public function getSignature($request_data): string
    {
        return base64_encode(hash_hmac(
            'sha1',
            implode('&', array(
                'POST',
                rawurlencode($this->getUrl()),
                rawurlencode(http_build_query($request_data, '', '&', PHP_QUERY_RFC3986)),
            )),
            implode('&', [$this->getApiSecret(), $this->getOauthSecret()]),
            true
        ));
    }

    /**
     * @throws Exception\RequestFail
     * @return \stdClass
     */
    public function result(): \stdClass
    {
        $request = $this->getRequestData();
        $request['oauth_signature'] = $this->getSignature($request);

        $curl = new Curl;
        $curl->post($this->getUrl(), http_build_query($request, '', '&'));

        if ($curl->error) {
            throw new Exception\RequestFail($curl->errorMessage);
        }

        $curl->close();

        return $curl->response;
    }
}