<?php
namespace MFTest\Application\Ingredients;


class Ingredients
{
    /**
     * @var array
     */
    protected $container = [];
    
    /**
     * @var \DateTime
     */
    protected $current_date;

    /**
     * @param \DateTime $current
     */
    public function __construct(\DateTime $current)
    {
        $this->current_date = $current;
    }
    
    /**
     * @param array $input
     * @return Ingredients
     */
    public function load(array $input): Ingredients
    {
        foreach ($input as $v)
        {
            $this->container[$v['title']] = (new Ingredient)
                ->setTitle($v['title'])
                ->setCurrentDate($this->current_date)
                ->setBestBefore(new \DateTime($v['best-before']))
                ->setUseBy(new \DateTime($v['use-by']))
                ->setStatus();
        }
        
        return $this;
    }
    
    public function get()
    {
        return $this->container;
    }
    
    /**
     * @param string $title
     * @return Ingredient
     */
    public function getIngredient(string $title): Ingredient
    {
        return $this->container[$title] ?? new Ingredient;
    }
}