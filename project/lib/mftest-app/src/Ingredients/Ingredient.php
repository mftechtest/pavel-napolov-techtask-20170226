<?php
namespace MFTest\Application\Ingredients;

class Ingredient
{
    const STATUS_OK = 'ok';
    const STATUS_NEAR_EXPIRED = 'near_expired';
    const STATUS_EXPIRED = 'expired';

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $status = '';

    /**
     * @var \DateTime
     */
    protected $current_date;

    /**
     * @var \DateTime
     */
    protected $best_before;

    /**
     * @var \DateTime
     */
    protected $use_by;

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return Ingredient
     */
    public function setTitle(string $title): Ingredient
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @throws Exception\DateRange
     * @return Ingredient
     */
    public function setStatus(): Ingredient
    {
        if ($this->getBestBefore()->getTimestamp() > $this->getUseBy()->getTimestamp()) {
            throw new Exception\DateRange('best-before greater than use-by');
        }
            
        if ($this->getUseBy()->getTimestamp() <= $this->getCurrentDate()->getTimestamp()) {
            $this->status = self::STATUS_EXPIRED;
        }
        else if ($this->getBestBefore()->getTimestamp() <= $this->getCurrentDate()->getTimestamp()) {
            $this->status = self::STATUS_NEAR_EXPIRED;
        }
        else {
            $this->status = self::STATUS_OK;
        }
			
        return $this;
    }

    /**
     * @throws Exception\ParamIsMissing
     * @return \DateTime
     */
    public function getCurrentDate(): \DateTime
    {
        if (!$this->current_date) {
            throw new Exception\ParamIsMissing('current_date');
        }
            
        return $this->current_date;
    }

    /**
     * @param \DateTime $current_date
     * @return Ingredient
     */
    public function setCurrentDate(\DateTime $current_date): Ingredient
    {
        $this->current_date = $current_date;

        return $this;
    }

    /**
     * @throws Exception\ParamIsMissing
     * @return \DateTime
     */
    public function getBestBefore(): \DateTime
    {
        if (!$this->best_before) {
            throw new Exception\ParamIsMissing('best_before');
        }
            

        return $this->best_before;
    }

    /**
     * @param \DateTime $best_before
     * @return Ingredient
     */
    public function setBestBefore(\DateTime $best_before): Ingredient
    {
        $this->best_before = $best_before;

        return $this;
    }

    /**
     * @throws Exception\ParamIsMissing
     * @return \DateTime
     */
    public function getUseBy(): \DateTime
    {
        if (!$this->use_by) {
            throw new Exception\ParamIsMissing('use_by');
        }
            

        return $this->use_by;
    }

    /**
     * @param \DateTime $use_by
     * @return Ingredient
     */
    public function setUseBy(\DateTime $use_by): Ingredient
    {
        $this->use_by = $use_by;

        return $this;
    }
}