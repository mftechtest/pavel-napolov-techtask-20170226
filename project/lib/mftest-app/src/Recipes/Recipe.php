<?php
namespace MFTest\Application\Recipes;

use MFTest\Application\Ingredients\Ingredient;

class Recipe implements \JsonSerializable 
{
    const STATUS_OK = 'ok';
    const STATUS_AVAILABLE = 'available';
    const STATUS_NOT_AVAILABLE = 'not_available';
    
    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $status = self::STATUS_OK;
    
    /**
     * @var array
     */
    protected $ingredients = [];

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return $this
     */
    public function setTitle(string $title): Recipe
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }
    
    /**
     *
     * @param Ingredient $current_ingredient
     * @return Recipe
     */
    public function setStatus(Ingredient $current_ingredient): Recipe
    {
        switch ($current_ingredient->getStatus())
        {
            case Ingredient::STATUS_NEAR_EXPIRED:
                $this->setStatusAvailable();
                break;
    
            case Ingredient::STATUS_EXPIRED:
    
            case '':
                $this->setStatusNotAvailable();
                break;
        }
    
        return $this;
    }
    
    /**
     * @return Recipe
     */
    public function setStatusAvailable(): Recipe
    {
        if ($this->status == self::STATUS_OK)
            $this->status = self::STATUS_AVAILABLE;
    
        return $this;
    }
    
    /**
     * @return Recipe
     */
    public function setStatusNotAvailable(): Recipe
    {
        $this->status = self::STATUS_NOT_AVAILABLE;
    
        return $this;
    }
    
    /**
     * @param array $ingredients
     * @return Recipe
     */
    public function setIngredients(array $ingredients): Recipe
    {
        $this->ingredients = $ingredients;

        return $this;
    }

    /**
     * @return array
     */
    public function getIngredients(): array
    {
        return $this->ingredients;
    }
    
    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        return [
            'title' => $this->title,
            'ingredients' => $this->ingredients
        ];
    }
}