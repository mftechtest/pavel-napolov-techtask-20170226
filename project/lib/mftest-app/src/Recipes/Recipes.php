<?php
namespace MFTest\Application\Recipes;

use MFTest\Application\Ingredients\Ingredients;

class Recipes 
{
    /**
     * @var array
     */
    protected $container = [];

    /**
     * @param array $input
     * @return Recipes
     */
    public function load(array $input): Recipes
    {
        foreach ($input as $v) {
            $this->container[] = (new Recipe)
                ->setTitle($v['title'])
                ->setIngredients($v['ingredients']);
        }
        
        return $this;
    }
    
    /**
     * @return array
     */
    public function get(): array
    {
        return $this->container;
    }
    
    /**
     * @param Ingredients $ingredients
     * @return array
     */
    public function filter(Ingredients $ingredients): array
    {
        $result = [];
        
        foreach ($this->container as $recipe) {
            foreach ($recipe->getIngredients() as $title) {
                $recipe->setStatus($ingredients->getIngredient($title));
            }
            
            $result[$recipe->getStatus()][] = $recipe;         
        }
        
        return array_merge($result[Recipe::STATUS_OK] ?? [], $result[Recipe::STATUS_AVAILABLE] ?? []);
    }
}