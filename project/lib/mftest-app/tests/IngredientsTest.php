<?php

use PHPUnit\Framework\TestCase;
use MFTest\Application\Ingredients\{Ingredients, Ingredient};

class IngredientsTest extends TestCase
{

    public function test_load()
    {
        $ingredients = new Ingredients(new \DateTime);

        $this->assertInstanceOf(Ingredients::class, $ingredients->load(json_decode('[{
            "title": "Ham",
            "best-before": "2017-02-25",
            "use-by": "2017-02-27"
        },
        {
            "title": "Cheese",
            "best-before": "2017-02-08",
            "use-by": "2017-02-13"
        }]', true)));

        $container = $ingredients->get();

        $this->assertInternalType('array', $container);
        $this->assertArrayHasKey('Ham', $container);
        $this->assertArrayHasKey('Cheese', $container);
    }

    public function test_ingredient()
    {
        $ingredient = (new Ingredients(new \DateTime))->load(json_decode('[{
            "title": "Ham",
            "best-before": "2017-02-25",
            "use-by": "2017-02-27"
        },
        {
            "title": "Cheese",
            "best-before": "2017-02-08",
            "use-by": "2017-02-13"
        }]', true))->getIngredient('Ham');

        $this->assertInstanceOf(Ingredient::class, $ingredient);
        $this->assertEquals('Ham', $ingredient->getTitle());
        $this->assertInstanceOf(\DateTime::class, $ingredient->getBestBefore());
        $this->assertEquals('2017-02-25', $ingredient->getBestBefore()->format('Y-m-d'));
        $this->assertInstanceOf(\DateTime::class, $ingredient->getUseBy());
        $this->assertEquals('2017-02-27', $ingredient->getUseBy()->format('Y-m-d'));
        $this->assertContains($ingredient->getStatus(), [Ingredient::STATUS_OK, Ingredient::STATUS_NEAR_EXPIRED, Ingredient::STATUS_EXPIRED]);
    }
}