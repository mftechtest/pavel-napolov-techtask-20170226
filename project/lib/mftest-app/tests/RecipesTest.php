<?php

use PHPUnit\Framework\TestCase;
use MFTest\Application\Recipes\{Recipes, Recipe};
use MFTest\Application\Ingredients\Ingredient;

class RecipesTest extends TestCase
{
    public function test_load()
    {
        $recipes = new Recipes;

        $this->assertInstanceOf(Recipes::class, $recipes->load(json_decode('[
            {
                "title": "Ham and Cheese Toastie",
                "ingredients": [
                    "Ham",
                    "Cheese",
                    "Bread",
                    "Butter"
                ]
            },
            {
                "title": "Fry-up",
                "ingredients": [
                    "Bacon",
                    "Eggs",
                    "Baked Beans",
                    "Mushrooms",
                    "Sausage",
                    "Bread"
                ]
            }]', true)));
        
        $container = $recipes->get();
        
        $this->assertInternalType('array', $container);
        $this->assertArrayHasKey(0, $container);
        $this->assertArrayHasKey(1, $container);
    }

    public function test_recipe()
    {
        $recipes = (new Recipes)->load(json_decode('[
            {
                "title": "Ham and Cheese Toastie",
                "ingredients": [
                    "Ham",
                    "Cheese",
                    "Bread",
                    "Butter"
                ]
            },
            {
                "title": "Fry-up",
                "ingredients": [
                    "Bacon",
                    "Eggs",
                    "Baked Beans",
                    "Mushrooms",
                    "Sausage",
                    "Bread"
                ]
            }]', true));
        
        $recipe = $recipes->get()[0];
        
        $this->assertInstanceOf(Recipe::class, $recipe);
        $this->assertInstanceOf(Recipe::class, $recipe->setStatus(new Ingredient));
        $this->assertEquals('Ham and Cheese Toastie', $recipe->getTitle());
        $this->assertInternalType('array', $recipe->getIngredients());
        $this->assertEquals(Recipe::STATUS_NOT_AVAILABLE, $recipe->getStatus());
    }
}