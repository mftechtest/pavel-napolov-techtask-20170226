<?php
namespace MFTest\HttpControllers;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\{Validation, Constraints as Assert};
use MFTest\Application\Ingredients\Ingredients;

class Recipes 
{
    /**
     * @param Request $request
     * @param Application $app
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function get(Request $request, Application $app)
    {
        $response = new Response();

        try {
            $this->validate($request->attributes->all()['_route_params']);
            $date = new \DateTime($request->get('date') ? $request->get('date') : null);
            $data = $app['app.recipes']->filter(
                $this->getIngredients($app, $date)
            );
            
            if ($request->get('tweet')) {
                $this->postTweet($app, $data, $date);
            }
            
            $response->setHttpCode(200)->setData($data);
            
        }
        catch (Exception\ConstraintViolation $e) {
            $response->setHttpCode(409)->setData([$e->getMessage()]);
        }
        
        return $app->json($response->toArray(), $response->getHttpCode());
    }
    
    /**
     * @param array $input
     * @throws Exception\ConstraintViolation
     * @return bool
     */
    public function validate(array $input): bool
    {
        $validator = Validation::createValidator();
         
        $errors = $validator->validate($input, [
            new Assert\Collection([
                'date' => new Assert\Optional([
                    new Assert\NotBlank,
                    new Assert\Date
                ]),

                'tweet' => new Assert\Optional([
                    new Assert\NotBlank
                ])
            ])
        ]);
         
        if (count($errors) === 0) {
            return true;
        }
        else {
            throw new Exception\ConstraintViolation((string)$errors);
        }
    }
    
    /**
     * @param Application $app
     * @param \DateTime $date
     * @return Ingredients
     */
    protected function getIngredients(Application $app, \DateTime $date): Ingredients
    {
        $call = $app['app.ingredients'];
        
        return $call($date);
    }
    
    /**
     * @param Application $app
     * @param array $data
     * @param \DateTime $date
     * @return string
     */
    protected function postTweet(Application $app, array $data, \DateTime $date): string
    {
        if (empty($data)) {
            $text = 'On '.$date->format('Y-m-d').' nothing to eat '.time();
        }
        else {
            $recipes = [];
            
            foreach ($data as $v) {
                $recipes[] = $v->getTitle();
            }
            
            $text = 'On '.$date->format('Y-m-d').' for lunch: '.implode(', ', $recipes).' '.time();
        }
        
        return $app['app.posttweet']
            ->setText($text)
            ->result()
            ->id_str;
    }
}
