<?php
namespace MFTest\HttpControllers;

class Response 
{
    /**
     * @var int
     */
    protected $http_code;

    /**
     * @var array
     */
    protected $data = [];

    /**
     * @return int
     */
    public function getHttpCode(): int
    {
        return $this->http_code;
    }

    /**
     * @param int $http_code
     * @throws \InvalidArgumentException
     * @return Response
     */
    public function setHttpCode(int $http_code): Response
    {
    	if (empty($http_code)) {
            throw new \InvalidArgumentException('http_code');
        }
    	
        $this->http_code = $http_code;

        return $this;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        switch ($this->http_code) {
            case 200:
                return 'OK';
            case 201:
                return 'OK';
            default:
                return 'fail';
        }
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     * @return $this
     */
    public function setData(array $data): Response
    {
        $this->data = $data;

        return $this;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'status' => $this->getStatus(),
            'code' => $this->getHttpCode(),
            'data' => $this->getData(),
        ];
    }
}