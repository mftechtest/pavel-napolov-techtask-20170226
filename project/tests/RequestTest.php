<?php

use PHPUnit\Framework\TestCase;
use Curl\Curl;

class RequestTest extends TestCase
{
    public function test_empty()
    {
        $curl = new Curl;
        $curl->get($GLOBALS['APPLICATION_URL'].'/lunch/2017-02-27');

        $this->assertEquals(200 , $curl->httpStatusCode);
        $this->assertEquals('application/json', $curl->responseHeaders['Content-Type']);
        $this->assertJsonStringEqualsJsonString(
            '{"status":"OK","code":200,"data":[]}',
            $curl->rawResponse
        );

        $curl->close();
    }

    public function test_response()
    {
        $curl = new Curl;
        $curl->get($GLOBALS['APPLICATION_URL'].'/lunch/2017-02-06');

        $this->assertEquals(200 , $curl->httpStatusCode);
        $this->assertEquals('application/json', $curl->responseHeaders['Content-Type']);
        $this->assertObjectHasAttribute('data', $curl->response);
        $this->assertInternalType('array', $curl->response->data);
        $this->assertArrayHasKey(0, $curl->response->data);
        $this->assertInstanceOf(stdClass::class, $curl->response->data[0]);
        $this->assertObjectHasAttribute('title', $curl->response->data[0]);
        $this->assertObjectHasAttribute('ingredients', $curl->response->data[0]);

        $curl->close();
    }
}