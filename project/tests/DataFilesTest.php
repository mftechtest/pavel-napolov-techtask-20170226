<?php

use Dotenv\Dotenv;
use PHPUnit\Framework\TestCase;

class DataFilesTest extends TestCase
{
    public function __construct()
    {
        (new Dotenv($GLOBALS['PROJECT_ROOT']))->load();
        parent::__construct();
    }

    public function test_exist()
    {
        $this->assertFileExists(getenv('JSON_FILE_INGREDIENTS'));
        $this->assertFileExists(getenv('JSON_FILE_RECIPES'));
    }

    public function test_access()
    {
        $this->assertFileIsReadable(getenv('JSON_FILE_INGREDIENTS'));
        $this->assertFileIsReadable(getenv('JSON_FILE_RECIPES'));
    }

    public function test_format()
    {
        $ingredients = json_decode(file_get_contents(getenv('JSON_FILE_INGREDIENTS')), true);
        $recipes = json_decode(file_get_contents(getenv('JSON_FILE_RECIPES')), true);

        $this->assertInternalType('array', $ingredients);
        $this->assertInternalType('array', $recipes);

        $this->assertArrayHasKey('ingredients', $ingredients);
        $this->assertArrayHasKey('recipes', $recipes);

        $this->assertInternalType('array', $ingredients['ingredients']);
        $this->assertInternalType('array', $recipes['recipes']);

        $this->assertArrayHasKey(0, $ingredients['ingredients']);
        $this->assertArrayHasKey(0, $recipes['recipes']);

        foreach (['title', 'best-before', 'use-by'] as $v)
            $this->assertArrayHasKey($v, $ingredients['ingredients'][0]);

        foreach (['title', 'ingredients'] as $v)
            $this->assertArrayHasKey($v, $recipes['recipes'][0]);

        $this->assertInternalType('array', $recipes['recipes'][0]['ingredients']);
    }
}