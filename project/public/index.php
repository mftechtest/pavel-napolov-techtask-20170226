<?php
require_once __DIR__.'/../vendor/autoload.php';

use Dotenv\Dotenv;
use Silex\Application;
use MFTest\Application\Ingredients\Ingredients;
use MFTest\Application\Recipes\Recipes;
use MFTest\TwitterClient\PostTweet;

(new Dotenv(__DIR__.'/../'))->load();
$silex = new Application;

$silex['debug'] = true;

$silex['app.recipes'] = (new Recipes)->load(
    json_decode(file_get_contents(getenv('JSON_FILE_RECIPES')), true)['recipes']
);

$silex['app.ingredients'] = $silex->protect(function ($date) {
    return (new Ingredients($date))->load(
        json_decode(file_get_contents(getenv('JSON_FILE_INGREDIENTS')), true)['ingredients']
    );
});

$silex['app.posttweet'] = (new PostTweet)
    ->setUrl(getenv('TWITTER_POST_URL'))
    ->setApiKey(getenv('TWITTER_API_KEY'))
    ->setApiSecret(getenv('TWITTER_API_SECRET'))
    ->setOauthKey(getenv('TWITTER_ACCESS_TOKEN'))
    ->setOauthSecret(getenv('TWITTER_ACCESS_SECRET'));

$silex->get('/', function () use ($silex) { return $silex->json(['status' => 'OK']);});

$silex->get('/lunch', 'MFTest\\HttpControllers\\Recipes::get');
$silex->get('/lunch/{date}', 'MFTest\\HttpControllers\\Recipes::get');

$silex->run();