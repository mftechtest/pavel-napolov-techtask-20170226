# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure("2") do |config|
  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.

  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://atlas.hashicorp.com/search.
  config.vm.box = "ubuntu/xenial32"
  config.vm.hostname = "mftest.local"
  config.vm.network :forwarded_port, guest: 80, host: 8989

  # Disable automatic box update checking. If you disable this, then
  # boxes will only be checked for updates when the user runs
  # `vagrant box outdated`. This is not recommended.
  # config.vm.box_check_update = false

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.
  # config.vm.network "forwarded_port", guest: 80, host: 8080

  # Create a private network, which allows host-only access to the machine
  # using a specific IP.
  # config.vm.network "private_network", ip: "10.52.0.52"

  # Create a public network, which generally matched to bridged network.
  # Bridged networks make the machine appear as another physical device on
  # your network.
  # config.vm.network "public_network"

  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.
  # config.vm.synced_folder "../data", "/vagrant_data"

  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  # Example for VirtualBox:
  #
  config.vm.provider "virtualbox" do |vb|
      # vb.cpus = 1
  #   # Display the VirtualBox GUI when booting the machine
  #   vb.gui = true
  #
  #   # Customize the amount of memory on the VM:
  #   vb.memory = "1024"
  end
  #
  # View the documentation for the provider you are using for more
  # information on available options.

  # Define a Vagrant Push strategy for pushing to Atlas. Other push strategies
  # such as FTP and Heroku are also available. See the documentation at
  # https://docs.vagrantup.com/v2/push/atlas.html for more information.
  # config.push.define "atlas" do |push|
  #   push.app = "YOUR_ATLAS_USERNAME/YOUR_APPLICATION_NAME"
  # end

  # Enable provisioning with a shell script. Additional provisioners such as
  # Puppet, Chef, Ansible, Salt, and Docker are also available. Please see the
  # documentation for more information about their specific syntax and use.
  config.vm.provision "shell", inline: <<-SHELL
     apt-get update && 
     apt-get dist-upgrade -y
     apt-get install -y git zip nginx php php-fpm php-curl php-zip php-xml php-mbstring
     echo 'Installing composer'
     php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
     php -r "if (hash_file('SHA384', 'composer-setup.php') === '55d6ead61b29c7bdee5cccfb50076874187bd9f21f65d8991d46ec5cc90518f447387fb9f76ebae1fbbacf329e583e30') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
     php composer-setup.php
     php -r "unlink('composer-setup.php');"
     mv composer.phar /usr/local/bin/composer
     echo "Setting up silex user ..."
     adduser --disabled-password --gecos "silex user" silex
     usermod -a -G silex www-data
     echo 'Copying nginx config ...'
     cp /vagrant/provision/mftest.conf /etc/nginx/conf.d/
     echo 'Restarting nginx ...'
     systemctl restart nginx
     systemctl status nginx
     echo 'Copying project files ...'
     mkdir -p /home/silex/mftest/public
     cp -R /vagrant/project/data /home/silex/mftest
     cp -R /vagrant/project/lib /home/silex/mftest
     cp -R /vagrant/project/tests /home/silex/mftest
     cp /vagrant/project/composer.json /home/silex/mftest
     cp /vagrant/project/phpunit.xml /home/silex/mftest
     cp /vagrant/project/.env /home/silex/mftest
     cp /vagrant/project/public/index.php /home/silex/mftest/public
     chown -R silex:silex /home/silex/mftest
     echo "Installing composer packages ..."
     su - silex -c 'cd mftest && composer install'
     echo "Running tests ..."
     su - silex -c 'cd mftest && vendor/phpunit/phpunit/phpunit' 
  SHELL
end
